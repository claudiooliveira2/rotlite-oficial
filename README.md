[TOC]

# Saber que a sincronização terminou

## Implementando o método onSync do Model

```
public class Object<T extends RotLiteObject> extends RotLiteObjectModel{
    @Override
    public void onSync(List synchronizedList) {
        super.onSync(synchronizedList);
        Log.v(TAG, "Sincronizou tudo! " + synchronizedList.size());
    }

    @Override
    public void onSync(List synchronizedList, List unsynchronizedList) {
        super.onSync(synchronizedList, unsynchronizedList);

        Log.v(TAG, "Sincronizou mas ainda tem dados para sincronizar! " + synchronizedList.size() + "/" + unsynchronizedList.size());
    }
}
```

## Via Receivers

```
@Override
protected void onResume() {
	// Intent filter padrão de fim de sincronização
	IntentFilter filter = new IntentFilter(RotLite.BROADCAST_SYNC_ACTION);
	//Vamos receber notificações específicas do objeto ObjectRotLiteModel
	LocalBroadcastManager.getInstance(this).registerReceiver(ObjectRotLiteModel.getReceiver(new RotLiteBroadcastReceiver() {
	    @Override
	    public void onFinishDownloadSync() {
              //Download terminado    
	    }

	    @Override
	    public void onFinishUploadSync() {
	      //Upload completo  
	    }
	}), filter);
}
```

# Sincronizandos grandes quantidades de código

Caso os endpoints de obtenção de dados, nos retornem muitos dados, o processo pode falhar em celulares com baixa memória. Para evitar esse problema, devemos usar a paginação do endpoint REST.

## Parametros REST

Para paginarmos a sincronização, devemos usar 2 parametros em nosso endpoint GET

- Página - Define qual a página devemos buscar
- Quantidade por página - Qual a quantidade de registros por página.

Definindo isso em nosso modelo

```
@Table(perPageQuery="perPage", pageQuery="page", perPageCount=100)
public class MyObject<T extends RotLiteObject> extends RotLiteObjectModel {
    // Definição da classe
}
```

*output*
```
GET http://.../endpoint?perPage=100&page=1
```
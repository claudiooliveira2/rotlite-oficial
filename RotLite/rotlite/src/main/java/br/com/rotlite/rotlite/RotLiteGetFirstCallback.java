package br.com.rotlite.rotlite;

/**
 * Created by claudio on 01/11/15.
 */
public interface RotLiteGetFirstCallback<T extends RotLiteObject> {
    void done(T model);
    void error(RotLiteException e);
}

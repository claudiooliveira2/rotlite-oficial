package br.com.rotlite.rotlite;


import java.util.List;

import okhttp3.Response;

/**
 * Created by claudio on 24/09/15.
 */
public interface RotLiteSyncCallback<T extends RotLiteObject> {
    void onSuccessLocal(T obj);
    void onFailureLocal(RotLiteException e);
    void onSuccessCloud(Response response, T obj);
    void onFailureCloud(RotLiteException e);
}
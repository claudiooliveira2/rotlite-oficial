package br.com.rotlite.rotlite;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by claudio on 28/10/15.
 */
public class RotLiteQuery<T extends RotLiteObject> implements RotLiteQueryInterface {

    public static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private final String ROTLITE_IS_SYNC = "rotlite_is_sync";
    public String DEFAULT_ID_FIELD_NAME = "uuid";

    private final OkHttpClient client;
    private final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private Table table;
    public String name, className;
    ContentValues content;
    String TAG = "RotLiteObject";
    String TAG_ACTIVITY = "RotLiteObjectActivity";

    private static Context context;
    private Class<T> classInstance;
    public static String classInstanceName;
    private SQLiteDatabase db;

    private String where = "1 = 1";
    private int limitMin = 0;
    private int limitMax = 0;
    private String limit = "";
    private String query = "";
    private String order = "";
    private String group = "";
    private String url = "";
    private String endpoint = "";
    private String endpointPost = "";
    private String endpointPut = "";
    private String endpointDelete = "";
    private boolean autosync = false;
    private int from;
    private boolean getById = false;

    //Define a páginação atual da busca
    private int currentPage = 1;

    public static RotLiteBroadcastReceiver broadcastReceiver;
    public String bodyDataFormat = RotLite.BODY_DATA_JSON;
    private boolean updateWebError = false;

    private RotLiteConverter converter;
    public SharedPreferences sPrefs;

    private Map<String, ForeignKeyContent> foreignKeys = new HashMap<String, ForeignKeyContent>();
    private List<String> include = new ArrayList();
    public Map<String, T> includedModels = new HashMap<String, T>();
    Map<String, String> dataType = new HashMap<String, String>();

    public static int REQUEST_POST_MODE = 1;
    public static int REQUEST_GET_MODE = 2;
    public int requestMode = 1;

    public RotLiteQuery(Context context, Class<T> clzz) {
        client = RotLiteUtils.newOkHttpClient();
        this.context = context;
        db = RotLite.getInstance().getDataBase(context);
        url = RotLiteUtils.getInstance().getServer(context);
        classInstance = clzz;
        classInstanceName = clzz.getName();
        className = this.classInstance.getName();

        if (this.classInstance.isAnnotationPresent(Table.class)) {

            Annotation annotation = this.classInstance.getAnnotation(Table.class);
            table = (Table) annotation;

            this.name = table.name();
            this.endpoint = table.endpoint();
            this.endpointPost = table.endpointPost();
            this.endpointPut = table.endpointPut();
            this.endpointDelete = table.endpointDelete();
            this.autosync = table.autosync();

            sPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        }

        for(Field field : this.classInstance.getDeclaredFields()){
            Class type = field.getType();
            String name = field.getName();
            Annotation[] annotations = field.getDeclaredAnnotations();

            if (field.isAnnotationPresent(ForeignKey.class)) {

                Class superClass = type.getSuperclass();

                if (type.isAssignableFrom(RotLiteObject.class) || superClass.equals(RotLiteObject.class) || superClass.getSuperclass().isAssignableFrom(RotLiteObject.class)) {

                    for (Annotation annotation : annotations) {

                        ForeignKey fk = (ForeignKey) annotation;
                        foreignKeys.put(fk.column(), new ForeignKeyContent(type, fk.column(), fk.references(), this.context));

                    }

                }
            }
        }

        this.from = RotLiteConsts.FROM_LOCAL;
    }

    @Override
    public void fromLocal() {
        this.from = RotLiteConsts.FROM_LOCAL;
    }

    @Override
    public void fromWeb() {
        this.from = RotLiteConsts.FROM_WEB;
    }

    @Override
    public void setEndpoint(String name) {
        endpoint = name;
    }

    @Override
    public void setEndpointPost(String name) {
        endpointPost = name;
    }

    @Override
    public void setEndpointPut(String name) {
        endpointPut = name;
    }

    @Override
    public void setEndpointDelete(String name) {
        endpointDelete = name;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public void limit(int max) {
        limitMax = max;
        limit = " LIMIT " + max;
    }

    @Override
    public void limit(int min, int max) {
        limitMin = min;
        limitMax = max;
        limit = " LIMIT " + min + ", " + max;
    }

    @Override
    public String getExecutedQuery() {
        return query;
    }

    @Override
    public void include(String column) {
        if (foreignKeys.containsKey(column)) {

            include.add(column);

        }else{
//            Log.e(TAG, "A coluna '" + column + "' não possui uma Foreign Key definida");
        }
    }

    @Override
    public void order(String order) {
        this.order = " ORDER BY " + order;
    }

    @Override
    public void group(String column) {
        this.group = " GROUP BY " + column;
    }

    @Override
    public void where(String where) {
        this.where = where;
    }

    @Override
    public void getById(String uuid) {
        if (include.size() > 0)
            where = where + " AND " + name + "." + DEFAULT_ID_FIELD_NAME + " ='" + uuid + "'";
        else
            where = where + " AND " + DEFAULT_ID_FIELD_NAME + " ='" + uuid + "'";

        getById = true;
    }

    @Override
    public List<T> find() throws RotLiteException {

        final List<T> list = new ArrayList<>();

        String sql = makeSqlQuery();

        query = sql;

        if (this.from == RotLiteConsts.FROM_LOCAL) {

            try {

                final Cursor cursor = db.rawQuery(query, null);
                return findExecute(cursor, list, null);

            } catch (SQLiteDatabaseCorruptException e) {
                throw new RotLiteException("SQLiteDatabaseCorruptException " + e.getMessage() + "", e.hashCode());
            } catch (android.database.sqlite.SQLiteConstraintException e) {
                throw new RotLiteException("SQLiteConstraintException " + e.getMessage() + "", e.hashCode());
            } catch (SQLiteException e) {
                String error = e.getMessage();

                if (error.contains(RotLiteConsts.ROTLITE_ERROR_NO_SUCH_TABLE)) {
                    return new ArrayList<>(); //Se a tabela não existir, não há registros, então retorna uma lista vazia!
                } else {

                    throw new RotLiteException("SQLiteException " + e.getMessage() + "", e.hashCode());
                }
            } catch (Exception e) {
                throw new RotLiteException("Exception::: " + e.getMessage() + "", e.hashCode());
            }
        } else if (this.from == RotLiteConsts.FROM_WEB) {

            try {

                return findFromWebMethod(null, list);

            } catch (Exception e) {
                e.printStackTrace();
                throw new RotLiteException(e.getMessage(), e.hashCode());
            }

        } else {
            return null;
        }

    }

    @Override
    public RotLiteObject getFirst() throws RotLiteException {
        limit(1);
        List<T> models = find();
        return ((models != null && models.size() >= 1) ? models.get(0) : null);
    }

    @Override
    public void put(String key, String value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_STRING);
    }

    @Override
    public void put(String key, double value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_DOUBLE);
    }

    @Override
    public void put(String key, int value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_INTEGER);
    }

    @Override
    public void put(String key, long value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_LONG);
    }

    @Override
    public void put(String key, boolean value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_BOOLEAN);
    }

    @Override
    public void put(String key, JSONObject value) {

        put(key, value.toString());

    }

    @Override
    public void put(String key, JSONArray value) {

        put(key, value.toString());

    }

    @Override
    public void setPOSTRequestMode() {
        this.requestMode = REQUEST_POST_MODE;
    }

    @Override
    public void setGETRequestMode() {
        this.requestMode = REQUEST_GET_MODE;
    }

    @Override
    public void findInBackground(final RotLiteCallback callback) {
        final List<T> list = new ArrayList<>();

        String sql = "";
        try {
            sql = makeSqlQuery();
        } catch (RotLiteException e) {
            e.printStackTrace();
        }

        query = sql;

        if (this.from == RotLiteConsts.FROM_LOCAL) {

            try {

                final Cursor cursor = db.rawQuery(query, null);

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        try {
                            findExecute(cursor, list, callback);
                        } catch (RotLiteException e) {
                            e.printStackTrace();
//                            Log.e(TAG, e.getMessage());
                        }

                    }
                };
                new Thread(runnable).start();

            } catch (SQLiteDatabaseCorruptException e) {
                callback.error(new RotLiteException("SQLiteDatabaseCorruptException " + e.getMessage().toString() + "", e.hashCode()));
            } catch (android.database.sqlite.SQLiteConstraintException e) {
                callback.error(new RotLiteException("SQLiteConstraintException " + e.getMessage(), e.hashCode()));
            } catch (SQLiteException e) {
                String error = e.getMessage();
                e.printStackTrace();

                if (error.contains(RotLiteConsts.ROTLITE_ERROR_NO_SUCH_TABLE)) {
                    callback.done(new ArrayList<>()); //Se a tabela não existir, não há registros, então retorna uma lista vazia!
                } else {
                    callback.error(new RotLiteException("SQLiteException.. " + e.getMessage(), e.hashCode()));
                }

            } catch (Exception e) {
                callback.error(new RotLiteException("Exception " + e.getMessage(), e.hashCode()));
            }
        } else if (this.from == RotLiteConsts.FROM_WEB) {

            try {

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {

                        try {
                            findFromWebMethod(callback, list);
                        } catch (IOException e) {
                            e.printStackTrace();
                            callback.error(new RotLiteException(e.getMessage(), e.hashCode()));
                        }

                    }
                };
                new Thread(runnable).start();

            } catch (Exception e) {
                e.printStackTrace();
                callback.error(new RotLiteException(e.getMessage(), e.hashCode()));
            }

        }
    }


    /* Métodos privados */

    @NonNull
    private String makeSqlQuery() throws RotLiteException {
        String sql = "SELECT * FROM ";
        String sqlTableNames = "";
        String aliases = "";

        if (include.size() > 0) {

            //sql = sql + name + ".* AS " + name + "_cm.*,";
            sqlTableNames = sqlTableNames + name + ",";
            String join = "";

            for (int i = 0; i < include.size(); i++) {

                String column = include.get(i);

                ForeignKeyContent fk = foreignKeys.get(column);

                sqlTableNames = sqlTableNames + fk.getTableName() + ",";
                join = join + fk.getTableName() + " ON " + name + "." + column + " = " + fk.getTableName() + "." + fk.references + " OR";

            }

            sqlTableNames = sqlTableNames.substring(0, sqlTableNames.length() - 1);

            try {

                join = join.substring(0, join.length() - 2);
                sql = sql + sqlTableNames;

                Map<String, List<String>> tablesColumns = new HashMap<>();

                Cursor getColumnsCursor = db.rawQuery(sql + " LIMIT 1", null);
                if (getColumnsCursor.moveToFirst()) {
                    do {
                        String[] cols = getColumnsCursor.getColumnNames();
                        tablesColumns = getTablesColumns(cols);
                    }while(getColumnsCursor.moveToNext());

                    for (Map.Entry<String, List<String>> entry : tablesColumns.entrySet()) {

                        String table = entry.getKey();
                        List<String> columns = entry.getValue();

                        for (String col : columns) {

                            aliases = aliases + table + "." + col + " AS `" + table + "_rotlite_column_" + col + "`,";

                        }

                    }

                }

                aliases = aliases.substring(0, aliases.length() - 1);

                sql = "SELECT " + aliases + " FROM " + name + " LEFT JOIN " + join + " WHERE " + where;

            }catch (Exception e) {
                throw new RotLiteException(e.getMessage(), e.hashCode());
            }

        }else{

            sql = sql + name + " WHERE " + where;

        }

        if (order != null && !order.equals("")) {
            sql = sql + order;
        }

        if (limit != null && !limit.equals("")) {
            sql = sql + limit;
        }

        if (group != null && !group.equals("")) {
            sql = sql + group;
        }

        return sql;
    }

    private Map<String, List<String>> getTablesColumns(String[] cols) {
        Map<String, List<String>> tablesColumns = new HashMap<>();
        List columnsName = new ArrayList();

        String currentTable = name;
        List tables = new ArrayList();

        for (int z = 0; z < cols.length; z++) {

            String col = cols[z].replace(currentTable + "_rotlite_column_", "");

            if (include.size() > 0) {
                if (col.equals("_id") || col.contains("rotlite_column__id")) {
                    columnsName = new ArrayList();
                    if (tables.size() > 0) {

                        int getNextTablePos = tables.size() - 1;
                        int i = 0;

                        for (Map.Entry<String, ForeignKeyContent> entry : foreignKeys.entrySet()) {
                            if (i == getNextTablePos) {
                                ForeignKeyContent fk = entry.getValue();
                                currentTable = fk.getTableName();
                            }
                            i++;
                        }

                    }else{

                    }

                    columnsName.add(col.replace(currentTable + "_rotlite_column_", ""));
                    tablesColumns.put(currentTable, columnsName);
                    tables.add(currentTable);

                }else{
                    if (tablesColumns.containsKey(currentTable)) {

                        List tbColumns = tablesColumns.get(currentTable);
                        tbColumns.add(col.replace(currentTable + "_rotlite_column_", ""));

                    }
                }
            }

        }

        return tablesColumns;
    }

    private List<T> findExecute(Cursor cursor, List<T> list, RotLiteCallback callback) throws RotLiteException {

        int pos = 0;
        if (cursor.moveToFirst()) {

            do {

                /**
                 * Observações: Quando houver ForeignKeys, as primeiras colunas até o segundo "_id"
                 * pertencem à tabela principal, as seguintes pertencerão âs tabelas relacionadas,
                 * sendo que cada "_id" é o indicador de que as próximas colunas pertencem â outras tabelas.
                 *
                 * Por exemplo, se executarmos o Log abaixo com foreign key em execução, notaremos
                 * o nome das colunas de todas as tabelas relacionadas e este é o método de identificar
                 * a quais tabelas pertencem estas colunas.
                 */

                String[] cols = cursor.getColumnNames();

                if (include.size() > 0) {

                    Map<String, List<String>> tablesColumns = getTablesColumns(cols);

                    T obj = null;

                    for (Map.Entry<String, List<String>> entry : tablesColumns.entrySet()) {

                        String table = entry.getKey();
                        List<String> columns = entry.getValue();

                        try {

                            for (Map.Entry<String, ForeignKeyContent> entry2 : foreignKeys.entrySet()) {
                                ForeignKeyContent fk = entry2.getValue();
                                String tb = fk.getTableName();
                                if (tb.equals(table) && !table.equals(name)) {
                                    obj = (T) fk.getModel();
//                                    Log.v(TAG, "OPA " + table);
                                }else if (table.equals(name)){
                                    obj = classInstance.getDeclaredConstructor(Context.class).newInstance(context);
                                }

                            }

                            obj.setTbName(table);

                            for (int i = 0; i < columns.size(); i++) {

                                String column = columns.get(i);
                                String colFormat = table + "_rotlite_column_" + column;
//                                Log.v(TAG, "column: " + colFormat + "; index: " + cursor.getColumnIndex(colFormat) + "; str: " + columns.get(i) + "; value: " + cursor.getString(cursor.getColumnIndex(colFormat)));

                                if (column.equals("_id")) obj.setLocalId(cursor.getInt(cursor.getColumnIndex(colFormat)));

                                switch (cursor.getType(i)) {
                                    case Cursor.FIELD_TYPE_NULL:
                                        obj.putNull(column.replace("_rotlite_column_", ""));
                                        break;
                                    case Cursor.FIELD_TYPE_BLOB:
                                        obj.put(column.replace("_rotlite_column_", ""), cursor.getString(cursor.getColumnIndex(colFormat)));
                                        break;
                                    case Cursor.FIELD_TYPE_FLOAT:
                                        obj.put(column.replace("_rotlite_column_", ""), cursor.getFloat(cursor.getColumnIndex(colFormat)));
                                        break;
                                    case Cursor.FIELD_TYPE_INTEGER:
                                        obj.put(column.replace("_rotlite_column_", ""), cursor.getInt(cursor.getColumnIndex(colFormat)));
                                        break;
                                    case Cursor.FIELD_TYPE_STRING:
                                        String str = cursor.getString(cursor.getColumnIndex(colFormat));
                                        if (isValidJSONArray(str)) {
                                            try {
                                                obj.put(column.replace("_rotlite_column_", ""), new JSONArray(str));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } else if (isValidJSONObject(str)) {
                                            try {
                                                obj.put(column.replace("_rotlite_column_", ""), new JSONObject(str));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            obj.put(column.replace("_rotlite_column_", ""), cursor.getString(cursor.getColumnIndex(colFormat)));
                                        }
                                        break;
                                }
                            }

                            if (table.equals(name)) { list.add(obj); } else {

                                for (Map.Entry<String, ForeignKeyContent> entry2 : foreignKeys.entrySet()) {
                                    ForeignKeyContent fk = entry2.getValue();
                                    String tb = fk.getTableName();
                                    if (tb.equals(table)) {
                                        list.get(pos).includedModels.put(fk.getColumn(), obj);
                                    }

                                }

                            }

                        } catch (InstantiationException e) {
//                            Log.e(TAG, "InstantiationException: " + e.getMessage());
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
//                            Log.e(TAG, "IllegalAccessException: " + e.getMessage());
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
//                            Log.e(TAG, "NoSuchMethodException: " + e.getMessage());
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
//                            Log.e(TAG, "InvocationTargetException: " + e.getMessage());
                            e.printStackTrace();
                        }

                    }

                }else{

                    T obj;
                    try {
                        obj = classInstance.getDeclaredConstructor(Context.class).newInstance(context);

                        obj.setId(cursor.getString(1));
                        obj.setLocalId(cursor.getInt(0));
                        obj.setTbName(name);

//                        //Log.v(TAG, "START COLUMN NAME ===============");

                        for (int i = 0; i < cols.length; i++) {

                            String column = cols[i].replace(name + "_rotlite_column_", "");

//                            //Log.v(TAG, "COlUMN NAME: " + cursor.getColumnName(i));

                            switch (cursor.getType(i)) {
                                case Cursor.FIELD_TYPE_NULL:
                                    obj.putNull(column);
                                    break;
                                case Cursor.FIELD_TYPE_BLOB:
                                    obj.put(column, cursor.getString(i));
                                    break;
                                case Cursor.FIELD_TYPE_FLOAT:
                                    obj.put(column, cursor.getFloat(i));
                                    break;
                                case Cursor.FIELD_TYPE_INTEGER:
                                    obj.put(column, cursor.getInt(i));
                                    break;
                                case Cursor.FIELD_TYPE_STRING:
                                    String str = cursor.getString(i);
                                    if (isValidJSONArray(str)) {
                                        try {
                                            obj.put(column, new JSONArray(str));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (isValidJSONObject(str)) {
                                        try {
                                            obj.put(column, new JSONObject(str));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        obj.put(column, cursor.getString(i));
                                    }
                                    break;
                            }
                        }
                        list.add(obj);
                    } catch (InstantiationException e) {
//                        Log.e(TAG, "InstantiationException: " + e.getMessage());
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
//                        Log.e(TAG, "IllegalAccessException: " + e.getMessage());
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
//                        Log.e(TAG, "NoSuchMethodException: " + e.getMessage());
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
//                        Log.e(TAG, "InvocationTargetException: " + e.getMessage());
                        e.printStackTrace();
                    }

                }

                pos++;

            } while (cursor.moveToNext());

        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        if (callback != null) {
            callback.done(list);
            return new ArrayList();
        } else {
            return list;
        }
    }

    private boolean isValidJSONArray(String jsonArray) {

        try {
            if (jsonArray != null) {
                JSONArray array = new JSONArray(jsonArray);
                return true;
            }else{
                return false;
            }
        } catch (JSONException e) {
            return false;
        }

    }

    private boolean isValidJSONObject(String jsonObject) {

        try {
            if (jsonObject != null) {
                JSONObject array = new JSONObject(jsonObject);
                return true;
            }else{
                return false;
            }
        } catch (JSONException e) {
            return false;
        }

    }

    private List<T> findFromWebMethod(final RotLiteCallback callback, final List<T> list) throws IOException {
        currentPage = 1;
        return findFromWebMethod(callback, list, null);
    }

    private List<T> findFromWebMethod(final RotLiteCallback callback, final List<T> list, final List loadedModels) throws IOException {

        if (callback != null) {

            getData(jsonObjectToCloud(), new Callback() {

                @Override
                public void onFailure(Call call, IOException e) {
                    callback.error(new RotLiteException("Failure request: " + e.getMessage()));
                    return;
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (response != null) {

                        //Não curte o padrão de recebimento de dados da RotLite? Ok, aceitamos o seu formato também!
                        if (converter != null) {

                            //TODO: MADRUGA Inicia DAQUI
                            //Gson gson = new Gson();

                            if (response.isSuccessful()) {

                                final JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));
                                converter.setJsonReader(reader);

                                converter.getJsonReader(new RotLiteConverter.DataObjects() {
                                    @Override
                                    public void getData(JsonReader data) {
                                        List models = generateObjectsJsonReader(data, false);
                                        if (loadedModels != null) {
                                            models.addAll(loadedModels);
                                        }

                                        int count = models.size();
                                        if (table.perPageCount() != 0 && count == table.perPageCount()) {
                                            currentPage++;
                                            try {
                                                findFromWebMethod(callback, list, models);
                                            } catch (IOException e) {
                                                e.printStackTrace();
//                                                Log.e(TAG, e.getMessage());
                                            }
                                        } else {
                                            currentPage = 1;
                                            callback.done(models);
                                        }
                                    }
                                });
                                reader.close();

                            } else {

                                callback.error(new RotLiteException(response.message(), response.code()));

                            }

                        } else {

                            if (response.isSuccessful()) {

                                JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), "UTF-8"));

                                //Se recebermos um array do backend...

                                List modelsFromWeb = generateObjectsJsonReader(reader, false);

                                if (loadedModels != null)
                                    modelsFromWeb.addAll(loadedModels);

                                int count = modelsFromWeb.size();

                                if (table.perPageCount() != 0 && count == table.perPageCount()) {
                                    currentPage++;
                                    findFromWebMethod(callback, list, modelsFromWeb);
                                } else {
                                    currentPage = 1;
                                    callback.done(modelsFromWeb);
                                }

                                reader.close();

                            } else {
                                String message = response.message();
                                if (response.code() == 404) {
                                    message = message + ": " + response.request().url().toString();
                                }else{
                                    message = response.body().string();
                                }
                                callback.error(new RotLiteException(message, response.code()));
                                return;
                            }
                        }
                    }else{
                        callback.error(new RotLiteException("EndPoint GET is not defined. Nothing to download."));
                    }
                }

            });
        } else {
            Call data = getData(jsonObjectToCloud(), null);
            if (data != null) {
                Response response = data.execute();
                if (response.isSuccessful()) {

                    String responseStr = response.body().string();

                    JsonParser jsonParser = new JsonParser();

                    try {

                        JsonArray arrayData = jsonParser.parse(responseStr).getAsJsonArray();

                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage());
                    }
                } else {
                    return new ArrayList<>();
                }
            }else{
                return new ArrayList<>();
            }
        }
        return new ArrayList<>();
    }

    private List generateObjectsJsonReader(JsonReader reader, boolean saveDataGenerated) {

        List<T> list = new ArrayList<>();

        try {

            if (reader.peek() == JsonToken.BEGIN_ARRAY) {
                reader.beginArray();
                while (reader.hasNext()) {
                    if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                        try {
                            list.add(generateObjectJsonReader(reader, saveDataGenerated));
                        } catch (RotLiteException e) {
                            e.printStackTrace();
//                            Log.e(TAG, "Error on generate object from json reader: " + e.getMessage());
                        }
                    }
                }
                reader.endArray();
            }else if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                try {
                    list.add(generateObjectJsonReader(reader, saveDataGenerated));
                } catch (RotLiteException e) {
                    e.printStackTrace();
//                    Log.e(TAG, "Error on generate object from json reader (2): " + e.getMessage());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
//            Log.e(TAG, "Error on read array " + e.getMessage());
        }

        return list;

    }

    private T generateObjectJsonReader(JsonReader reader, boolean saveDataGenerated) throws RotLiteException {

        T obj = null;
        try {
            obj = classInstance.getDeclaredConstructor(Context.class).newInstance(context);

            try {

                reader.beginObject();

//                //Log.e(TAG1, "object ");
                while(reader.hasNext()) {

//                    //Log.v(TAG1, "Name: " + reader.peek().name());

                    String name = "";

//                    //Log.v(TAG1, "Get Peek: " + reader.peek());

                    if (reader.peek() == JsonToken.NAME)
                        name = reader.nextName();

                    if (reader.peek() != JsonToken.NULL) {
//                        //Log.v(TAG1, "Peek: " + reader.peek().name());

                        if (reader.peek() == JsonToken.STRING) {
//                            //Log.v(TAG1, "'" + name + "' String: " + reader.nextString());
                            obj.put(name, reader.nextString());
                        }

                        if (reader.peek() == JsonToken.BOOLEAN) {
//                            //Log.v(TAG1, "'" + name + "' Boolean: " + reader.nextBoolean());
                            obj.put(name, reader.nextBoolean());
                        }

                        if (reader.peek() == JsonToken.BEGIN_OBJECT) {
//                            //Log.v(TAG1, "'" + name + "' Object");
                            obj.put(name, getJSONObjectFromJsonReader(reader));
                        }

                        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
                            obj.put(name, getJSONArrayFromJsonReader(reader));
                        }

                        try {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(name, reader.nextInt());
                            }
                        }catch(NumberFormatException e) {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(name, reader.nextDouble());
                            }
                        }

                    }else{
                        obj.putNull(name);
                        reader.skipValue();
                    }

                }
                reader.endObject();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        if (obj != null && saveDataGenerated) {
            try {
                obj.setIsSync(true);
                obj.saveLocal();
            } catch (Exception e) {
                e.printStackTrace();
//                Log.e(TAG, "Error on saveLocal downloaded model: " + e.getMessage());
            }
        }

        return obj;

    }

    public static JSONObject getJSONObjectFromJsonReader(JsonReader reader) {

        final JSONObject obj = new JSONObject();

        try {
            reader.beginObject();
            while(reader.hasNext()) {

                try {

                    String name = "";

//                    //Log.v(TAG1, "Get Peek: " + reader.peek());

                    if (reader.peek() == JsonToken.NAME)
                        name = reader.nextName();

                    if (reader.peek() != JsonToken.NULL) {

                        if (reader.peek() == JsonToken.STRING) {
                            obj.put(name, reader.nextString());
                        }

                        if (reader.peek() == JsonToken.BOOLEAN) {
                            obj.put(name, reader.nextBoolean());
                        }

                        if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                            obj.put(name, getJSONObjectFromJsonReader(reader));
                        }

                        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
                            reader.skipValue();
                        }

                        try {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(name, reader.nextInt());
                            }
                        }catch(NumberFormatException e) {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(name, reader.nextDouble());
                            }
                        }

                    }else{
                        obj.put(name, null);
                        reader.skipValue();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return obj;

    }

    public static JSONArray getJSONArrayFromJsonReader(JsonReader reader) {

        final JSONArray obj = new JSONArray();

        try {
            reader.beginArray();
            while(reader.hasNext()) {

                try {

                    String name = "";

                    if (reader.peek() == JsonToken.NAME)
                        name = reader.nextName();

                    if (reader.peek() != JsonToken.NULL) {

                        if (reader.peek() == JsonToken.STRING) {
                            obj.put(reader.nextString());
                        }

                        if (reader.peek() == JsonToken.BOOLEAN) {
                            obj.put(reader.nextBoolean());
                        }

                        if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                            obj.put(getJSONObjectFromJsonReader(reader));
                        }

                        if (reader.peek() == JsonToken.BEGIN_ARRAY) {
                            obj.put(getJSONArrayFromJsonReader(reader));
                        }

                        try {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(reader.nextInt());
                            }
                        }catch(NumberFormatException e) {
                            if (reader.peek() == JsonToken.NUMBER) {
                                obj.put(reader.nextDouble());
                            }
                        }

                    }else{
                        reader.skipValue();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            reader.endArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return obj;

    }

    private Call getData(JSONObject object, Callback callback) throws IOException {

        FormBody.Builder form = new FormBody.Builder();

        if ((endpoint != null && !endpoint.equals("") && requestMode == REQUEST_GET_MODE) || (((endpointPost != null) && !endpointPost.equals("")) && requestMode == REQUEST_POST_MODE)) {
            String getUrl = url + endpoint + "?";

            if (object != null) {
                JSONArray names = object.names();

                for (int i = 0; i < object.length(); i++) {

                    String name = "";

                    try {

                        name = names.getString(i);

                        try {

                            if (requestMode == REQUEST_GET_MODE) getUrl = getUrl + name + "=" + URLEncoder.encode(object.getString(name), "UTF-8") + "&";
                            form.add(name, object.getString(name));

                        } catch (Exception e) {

                            try {

                                if (requestMode == REQUEST_GET_MODE) getUrl = getUrl + name + "=" + URLEncoder.encode(String.valueOf(object.getInt(name)), "UTF-8") + "&";
                                form.add(name, String.valueOf(object.getInt(name)));

                            } catch (Exception e2) {

                                try {
                                    if (requestMode == REQUEST_GET_MODE) getUrl = getUrl + name + "=" + URLEncoder.encode(String.valueOf(object.getDouble(name)), "UTF-8") + "&";
                                    form.add(name, String.valueOf(object.getDouble(name)));
                                } catch (Exception e3) {
                                    try {
                                        if (requestMode == REQUEST_GET_MODE) getUrl = getUrl + name + "=" + URLEncoder.encode(String.valueOf(object.getLong(name)), "UTF-8") + "&";
                                        form.add(name, String.valueOf(object.getLong(name)));
                                    } catch (Exception e4) {

                                        try {
                                            if (requestMode == REQUEST_GET_MODE) getUrl = getUrl + name + "=" + URLEncoder.encode(String.valueOf(object.getBoolean(name)), "UTF-8") + "&";
                                            form.add(name, String.valueOf(object.getBoolean(name)));
                                        } catch (Exception e5) {
//                                            Log.e(TAG, "Get value error: " + e5.getMessage());
                                            e5.printStackTrace();
                                        }

                                    }
                                }

                            }

                        }

                    } catch (JSONException e) {
//                        Log.e(TAG, "JSONException: " + e.getMessage());
                        e.printStackTrace();
                    }

                }

            }

            getUrl = getUrl.substring(0, getUrl.length() - 1);

            getUrl = appendQueryPage(getUrl);

            Request.Builder builder = new Request.Builder();

            if (requestMode == REQUEST_POST_MODE) {
                getUrl = url + endpointPost;
                RequestBody formBody = form.build();
                builder.post(formBody);
//                Log.v(TAG, "POST: " + getUrl);
            }else{
//                Log.v(TAG, "GET: " + getUrl);
            }

            builder.url(getUrl);

//            //Log.v(TAG, "URL: " + getUrl);

            Request request = builder.build();

            Call call = client.newCall(request);
            if (callback != null) {
                call.enqueue(callback);
            }
            return call;
        }else{
            callback.onResponse(null, null);
            return null;
        }
    }

    private String appendQueryPage(String getUrl) {
        if (table != null && !table.perPageQuery().equals("") && !table.pageQuery().equals("")){
            getUrl = getUrl + "&" + table.perPageQuery() + "=" + table.perPageCount() + "&" + table.pageQuery() + "=" + Integer.toString(currentPage);//TODO: Paginação, mudar as páginas
        }
        return getUrl;
    }

    private JSONObject jsonObjectToCloud() {

        if (content == null) return new JSONObject();

        JSONObject json = getDefaultJson();

        return json;
    }

    @NonNull
    protected JSONObject getDefaultJson() {
        JSONObject json = new JSONObject();

        Set<String> keys = content.keySet();

        for (String key : keys) {

            try {

                String type = dataType.get(key);

                if (type.equals(RotLiteConsts.DATA_TYPE_STRING) || type.equals(RotLiteConsts.DATA_TYPE_DATE) || type.equals(RotLiteConsts.DATA_TYPE_DATETIME)) {
                    String v = content.getAsString(key);

                    if (isValidJSONArray(v)) {
                        json.put(key, new JSONArray(v));
                    }else if (isValidJSONObject(v)) {
                        json.put(key, new JSONObject(v));
                    }else {
                        json.put(key, v);
                    }

                } else if (type.equals(RotLiteConsts.DATA_TYPE_INTEGER)) {
                    int v = content.getAsInteger(key);
                    json.put(key, v);
                } else if (type.equals(RotLiteConsts.DATA_TYPE_DOUBLE)) {
                    double v = content.getAsDouble(key);
                    json.put(key, v);
                } else if (type.equals(RotLiteConsts.DATA_TYPE_LONG)) {
                    long v = content.getAsLong(key);
                    json.put(key, v);
                } else if (type.equals(RotLiteConsts.DATA_TYPE_BOOLEAN)) {
                    boolean v = content.getAsBoolean(key);
                    json.put(key, v);
                }

            } catch (JSONException e) {
//                Log.e(TAG, "Get value error: " + e.getMessage());
            }

        }
        return json;
    }
}
